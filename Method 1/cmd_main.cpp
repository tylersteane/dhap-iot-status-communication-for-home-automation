/*
 ============================================================================
 Author      : Tyler Steane
 Description : CMD co-ordination script for method 1 only.
 Compile	 : g++ -o cmd cmd_main.cpp UDP_class.cpp -lpthread
 Usage		 :  ./cmd filename test_duration(sec) No.tests
 ============================================================================
 */
#include <iostream>
#include <cstdlib>
#include <sstream>
#include <fstream>
#include <pthread.h>
#include <time.h>
#include <sys/time.h>

#include "UDP_class.h"

using namespace std;


#define ICD1_IP "192.168.5.11"
#define ICD2_IP "192.168.5.12"
#define ICD3_IP "192.168.5.13"
#define ICD4_IP "192.168.5.14"

#define CMD_TX 55554 // I.e The ICD listner port, Current talker port.
#define CMD_RX CMD_TX+2

ofstream logfile;


struct UDPPort {
	string IP;
	int Port;
	string udp_pkt; // received message
	bool msg_waiting;// msg waiting flag.
};

void* ltnr(void* arg)
{
	struct UDPPort *config = (struct UDPPort *)arg;
	Tudp_handler rx_udp( "", config->Port) ;

	while(1){

		if (rx_udp.wait_receive_udp () )
		{//--- got an error
			//cout << "   Send error: " << rx_udp.error_message << endl << endl ;
			//return(-1) ;
			//pthread_exit(0);
			//continue;
		}
		//cout << "   Received packet: " << rx_udp.rcv_str << endl << endl ;

		config->udp_pkt = rx_udp.rcv_str;
		config->msg_waiting = true;

		}
}

bool send(Tudp_handler *tx, string msg ){
	tx->send_str = msg ;
	   if ( tx->send_udp() )
	     {//--- got an error
	        cout << "   Send error: " << tx->error_message << endl ;
	        return(false) ;
	     }
	   //cout << "   Target IP is " << tx->udp_ip << ",  port " << endl ;
	   cout << "   Sent packet: " << tx->send_str << endl << endl ;
	   return (true);
}

string int_to_string(int num){
	ostringstream ss;
	ss << num;
	return ss.str();
}


string to_string(char* str){
	ostringstream ss;
	ss << str;
	return ss.str();
}

bool start_log(string filename){
	logfile.close();//just in case
	logfile.open(filename.c_str());

	if (!logfile.is_open()){ // check file is open
		cout<< "Could not start Log"<<endl;
		return false;
	}
	logfile<< "Record of number of packets sent by ESP" << endl;
	return true;
}

bool write_log(string val){
	if (!logfile.is_open()){ // check file is open
		cout<< "Could not start Log"<<endl;
		return false;
	}
	cout << "write to file " << val << endl;
	logfile << val << "\n";
	return true;
}

bool stop_log(){
	logfile.close();
	return true;
}


int main(int argc, char **argv) {

	string cmd;
	long run_time; // time since test started
	struct timeval t_curr, t_last; // Used to calc need for refresh.
	struct timespec sleep_time ; // Time spec for sleeps.
	   sleep_time.tv_sec = 0 ;
	   sleep_time.tv_nsec = 0.5E3 ;// roughly works out to 1ms.


	//<<<<<<<<<< Setup UDP Rx
   //// UDP port Structs setup:
	struct UDPPort cmd_port;
		cmd_port.IP ="192.168.5.82";//ESP IP.
		cmd_port.Port = CMD_RX; // note method one is very different to other methods.
		cmd_port.udp_pkt = "";
		cmd_port.msg_waiting = false;

	//
	// Thread Set-up:
	//
	pthread_attr_t attr;	// Default attributes
	pthread_attr_init(&attr);
	pthread_t tid_cmd;// two Id's for threads
	pthread_create(&tid_cmd, &attr, ltnr, &cmd_port);


	Tudp_handler tx_udp_cmd(cmd_port.IP, CMD_TX) ;

	Tudp_handler tx_udp_icd1(ICD1_IP, CMD_TX);
	Tudp_handler tx_udp_icd2(ICD2_IP, CMD_TX);
	Tudp_handler tx_udp_icd3(ICD3_IP, CMD_TX);
	Tudp_handler tx_udp_icd4(ICD4_IP, CMD_TX);
	Tudp_handler *tx_udp_icds [] = {&tx_udp_icd1, &tx_udp_icd2, &tx_udp_icd3, &tx_udp_icd4};

	start_log(to_string(argv[1]).append(".txt"));

	long duration = atoi(argv[2])*1E6;// input minutes to micro seconds.

	long default_r_rate = 2 ;
	long first_r_rate[] = {default_r_rate, 10,  1 } ;// for default, low, high tests.

	//// Main Loop:

	int repeats = atoi(argv[3]);
	string filename = "method1";
	ostringstream ss;

	for( int rate =0; rate < 3; rate ++){
		for (int no_icds = 0; no_icds <4 ; no_icds++){
			ss.str(std::string());//clear string
			ss << "Default Rate: " << default_r_rate << " first ICD rate : "<< first_r_rate[rate]<< " No. ICD's Active: " << no_icds+1 << " No of Tests: " << repeats << ":" << endl;
			write_log(ss.str());
			string results;
			for (int tests=0; tests < repeats; tests++ ){

				gettimeofday(&t_last, NULL); // get current time for comparison later should be part of loop activation;

				// Start ICD's

				for (int icd =0; icd <= no_icds; icd++){
					ss.str(std::string());//clear string
					if(icd ==0){
						ss << "Start " << "ICD"<<(icd+1)<< "of"<<(no_icds+1) <<"-"<<filename << "-"<< rate <<"-" << tests << ".txt R"<<  first_r_rate[rate] ;
					}
					else{
						ss << "Start " << "ICD"<<(icd+1)<< "of"<<(no_icds+1) <<"-"<<filename << "-"<< rate <<"-" << tests << ".txt R"<< default_r_rate;
					}
						cmd =  ss.str();
					send(tx_udp_icds[icd], cmd);

				}
				// Start ESP:

				ss.str(std::string());//clear string
					ss << "Start " << filename << "-"<< default_r_rate <<"-" << tests << ".txt R"<< default_r_rate;
				cmd =  ss.str();

				//cout << "Target IP: " << cmd_port.IP << "Port: " <<(cmd_port.Port+2) << endl;
				//cout << "sending" << cmd << endl;

				send(&tx_udp_cmd, cmd);


				// sleep lots and wait for test to end

				while (1){

					nanosleep(&sleep_time,NULL); // don't hog the cpu.

					// Check Refresh status:
					gettimeofday(&t_curr, NULL);
					run_time = (t_curr.tv_sec  - t_last.tv_sec ) * 1000000 +
											(t_curr.tv_usec - t_last.tv_usec) ;

					if(run_time >= duration){
						cout <<  "test complete"<< endl;
						send(&tx_udp_cmd, "Stop");
						// stop active ICD's
						for (int icd =0; icd <=no_icds; icd++){send(tx_udp_icds[icd], "Stop");}

						send(&tx_udp_cmd, "Report");

						// catch an ignored report request:

						int i=0;
						int j = 0;
						while(1){
							if (i > 100000){
								cout << "Asking for another report"<<endl;
								send(&tx_udp_cmd, "Report");
								i = 0;
								j++;
							}
							if (j>=10){
								cout << "NO response Error!"<< endl;
								break;
							}

							if(cmd_port.msg_waiting){
								cout << "got report"<< endl;
								break;
							}
							nanosleep(&sleep_time,NULL); // don't hog the cpu.
							i++;
						}

						if (j < 10){ // i.e didn't time out.
							//write_log(cmd_port.udp_pkt);
							results.append(cmd_port.udp_pkt).append(",");
							cmd_port.msg_waiting =false;
						}
						else{
							//write_log("Error: No response!");
							results.append("NA").append(",");
						}
						break;

					}
				}// end refresh check

			}// tests
			write_log(results);
		} // No_ICDS
	} // Rates

	stop_log();

}//end main

# DHAP - IoT Status Communication for Home Automation

The code (and results) used to emulate and test the four Protocols Proposed in the paper "IoT Status Communication for Home Automation".

This repository makes available the code and results data obtained in the work presented in the paper "IoT Status Communication for Home Automation".

Code was complied using compiletc running on tiny core Linux. (see paper for more details.)

Please note the experiments run where between ICD's and IoT devices. When the code was written the IoT devices where expected to be implemented using ESP8266 chip based devices so IoT devices are refereed to as ESP's in much of the code.

Each protocol/method has its own stand-alone folder which has all the required code to emulate the method.
each method has 3 device types:
	-	ICD's (1-4 used in testing)
	-	ESP	 (1 used in testing)
	-	CMD (Only one needed.)
	
Each script is run on a seperate machine, with some machines running the same script.
Note that IP's where assigned statically and are assumed in the code.
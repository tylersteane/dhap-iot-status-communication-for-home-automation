/*
 ============================================================================
 Author      : Tyler Steane
 Description : Method 3 IoT/esp Script
 Compile	 : g++ -o esp esp_main.cpp UDP_class.cpp -lpthread
 Usage		 : run ./esp
 ============================================================================
 */

#include <iostream>
#include <cstdlib>
#include <sstream>
#include <fstream>
#include <pthread.h>
#include <time.h>
#include <sys/time.h>

#include "UDP_class.h"

using namespace std;


#define Refresh_Rate 10E6 //in us

#define PROT_TX 44444
#define PROT_RX	PROT_TX+2;

#define CMD_RX 	55554 // was 55556
#define CMD_TX	CMD_RX+2//CMD_RX+2
#define REPLY_MSG "Sync"
#define BRDCST_IP "192.168.5.255"





ofstream logfile;
int pkt_count = 0;

struct UDPPort {
	string IP;
	string src_IP;
	int Port;
	string udp_pkt; // received message
	bool  msg_waiting; // msg waiting flag.
};

struct Subscriptions{
	//string 	sub_ips;
	Tudp_handler *sub_Tudp;
	long	sub_r_rates;
	struct timeval t_last;
};

void* ltnr(void* arg)
{
	struct UDPPort *config = (struct UDPPort *)arg;
	Tudp_handler rx_udp( "", config->Port) ;

	while(1)
	{
		if (rx_udp.wait_receive_udp () )
		{//--- got an error
			//cout << "   Send error: " << rx_udp.error_message << endl << endl ;
			//return(-1) ;
			//pthread_exit(0);
			continue;
		}
		//cout << "   Received packet: " << rx_udp.rcv_str << endl << endl ;

		config->udp_pkt = rx_udp.rcv_str;
		config->src_IP = rx_udp.src_ip;
		rx_udp.rcv_str.clear();
		config->msg_waiting = true;
	}
}

bool send_prot(Tudp_handler *tx, string msg ){
	tx->send_str = msg ;
	   if ( tx->send_udp() )
	     {//--- got an error
	        cout << "   Send error: " << tx->error_message << endl ;
	        return(false) ;
	     }
	   //cout << "   Target IP is " << IP << ",  port " << PORT << endl ;
	   cout << "   Sent packet: " << tx->send_str << endl << endl ;

	   pkt_count++;

	   return (true);
}

bool send_cmd(Tudp_handler *tx, string msg ){
	tx->send_str = msg ;
	   if ( tx->send_udp() )
	     {//--- got an error
	        cout << "   Send error: " << tx->error_message << endl ;
	        return(false) ;
	     }
	   //cout << "   Target IP is " << IP << ",  port " << PORT << endl ;
	   cout << "   Sent packet: " << tx->send_str << endl << endl ;
	   return (true);
}


string int_to_string(int num){
	ostringstream ss;
	ss << num;
	return ss.str();
}


string to_string(char* str){
	ostringstream ss;
	ss << str;
	return ss.str();
}

bool start_log(string filename){
	logfile.close();//just in case
	logfile.open(filename.c_str());

	if (!logfile.is_open()){ // check file is open
		cout<< "Could not start Log"<<endl;
		return false;
	}
	logfile<< "Record of packet Latencey (in ms)"<< endl;
	return true;

}

bool write_log(long val){
		if (!logfile.is_open()){ // check file is open
		cout<< "Could not start Log"<<endl;
		return false;
	}
	cout << "write to file" << val << endl;
	logfile << val << "\n";
	//logfile.close();
	return true;
}

bool stop_log(){
	logfile.close();
	return true;
}


int main(int argc, char **argv) {





	struct refresh_rate{
		long rate;
		bool set;
	};

	struct refresh_rate r_rate;
		r_rate.rate = 0 ;
		r_rate.set =false;

	long status_age; // time since last refresh.
	struct timeval t_curr, t_last; // Used to calc need for refresh.
	struct timespec sleep_time ; // Time spec for sleeps.
	   sleep_time.tv_sec = 0 ;
	   sleep_time.tv_nsec = 0.5E3 ;// roughly works out to 1ms.


//<<<<<<<<<< Setup UDP Rx

 //// UDP port Structs setup:
	struct UDPPort cmd_port;
		cmd_port.IP = "192.168.5.100";
		cmd_port.src_IP = "";
		cmd_port.Port = CMD_RX;
		cmd_port.udp_pkt = "";
		cmd_port.msg_waiting = false;

	struct UDPPort protocol_port;
			protocol_port.IP = "";
			protocol_port.src_IP = "";
			protocol_port.Port = PROT_RX;
			protocol_port.udp_pkt = "";
			protocol_port.msg_waiting = false;

//
// Thread Set-up:
	pthread_attr_t attr;	// Default attributes
	pthread_attr_init(&attr);
	pthread_t tid_cmd, tid_p;// two Id's for threads

	pthread_create(&tid_cmd, &attr, ltnr, &cmd_port);
	pthread_create(&tid_p, &attr, ltnr, &protocol_port);


//<<<<<<<<<< Setup UDP Tx
	Tudp_handler tx_udp_protocol( BRDCST_IP, PROT_TX) ;
	Tudp_handler tx_udp_cmd(cmd_port.IP, CMD_TX) ;
//	cout << "load up:"<< cmd_port.IP<< endl;

//// Main Loop:

	// wait for a start signal
	bool active =false;



	while(1)
	{
		// Sleep to free up CPU for other tasks
				nanosleep(&sleep_time, NULL) ;

		// Check Command port (this may tell loop to stop or report status)
		if (cmd_port.msg_waiting){
			string pkt;
			pkt.clear();

			pkt = cmd_port.udp_pkt;
			cout << "cleared?:" << pkt<< endl;
			cmd_port.msg_waiting =false;
			cmd_port.udp_pkt.clear();

			cout << "RX from "<< cmd_port.Port<< " : "<<pkt<<endl;

			//tempale string: Start <filename> R<Refresh_Rate>
			//		EG: Start ICD1-1510 R10
			if(!pkt.compare("Start")){

				active = true;
				pkt_count =0;

				r_rate.rate = 1E9 ;
				r_rate.set =false;

				gettimeofday(&t_last, NULL);

				continue;
			}
			if(!pkt.compare("Stop")){
				//stop_log();
				active = false;
				continue;
			}
			if(!pkt.compare("Report")){
				//cout <<"Report!" <<cmd_port.IP<< " "<< (cmd_port.Port-2)<< endl;
				//int i = 255;
				send_cmd(&tx_udp_cmd, int_to_string(pkt_count));
			}

		}	// end command port check.

		// check that the program has been activated:
		if (!active){

			continue;
		}

		if (protocol_port.msg_waiting){
			string pkt = protocol_port.udp_pkt;
			protocol_port.msg_waiting =false;

			cout << "Prot RX from "<< protocol_port.Port<< " : "<< pkt.substr(0,3)<<endl;
			//template string: Sub R<Refresh_Rate(s)>
			//		EG: Start ICD1-1510 R10
			if(!pkt.substr(0,3).compare("Sub")){
				cout << "got sub from:"<< protocol_port.src_IP << endl;
				int pkt_rate =  atoi(pkt.substr(pkt.find("R")+1).c_str())*1E6;

 				if (pkt_rate < r_rate.rate){
 					r_rate.rate = pkt_rate;
 					cout <<"rate is now: "<< r_rate.rate<<endl;
					r_rate.set = true;
 				}
 				// Give first update.
				send_prot(&tx_udp_protocol, REPLY_MSG); // request refresh.
			}
		}//end Protocol port


		// Check Refresh status:
		gettimeofday(&t_curr, NULL);
		status_age = (t_curr.tv_sec  - t_last.tv_sec ) * 1000000 +
				        (t_curr.tv_usec - t_last.tv_usec) ;
		// check all subscriptions to see if need to be updated:
		if(r_rate.set && (status_age >=r_rate.rate)){
			cout <<  "Sync Update"<< endl;
			send_prot(&tx_udp_protocol, REPLY_MSG); // request refresh.
			gettimeofday(&t_last, NULL); // reset refresh.

		}// end refresh check

	}// end while

}//end main

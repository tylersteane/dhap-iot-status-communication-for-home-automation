/*
 ============================================================================
 Author      : Tyler Steane
 Description : Method 4 ICD Script
 Compile	 : g++ -o icd icd_main.cpp UDP_class.cpp -lpthread
 Usage		 : run ./icd protocol_ip
 ============================================================================
 */

#include <iostream>
#include <cstdlib>
#include <sstream>
#include <fstream>
#include <pthread.h>
#include <time.h>
#include <sys/time.h>

#include "UDP_class.h"

using namespace std;


#define Refresh_Rate 10E6 // in us
#define CMD_IP "192.168.5.100"
#define PROT_IP "192.168.5.82"

#define PROT_RX	44444
#define PROT_TX PROT_RX+2
#define CMD_RX 	55554
#define CMD_TX	CMD_RX+2

#define REPLY_MSG "Sync"


ofstream logfile;
int pkt_count = 0;

struct UDPPort {
	string IP;
	int Port;
	string udp_pkt; // received message
	bool  msg_waiting; // msg waiting flag.
};

void* ltnr(void* arg) // listner thread
{
	struct UDPPort *config = (struct UDPPort *)arg;
	Tudp_handler rx_udp( "", config->Port) ; // config->IP, config->Port) ;

	while(1)
	{
		if (rx_udp.wait_receive_udp () )
		{//--- got an error
			//cout << "   Send error: " << rx_udp.error_message << endl << endl ;
			//return(-1) ;
			//pthread_exit(0);
			continue;
		}
		//cout << "   Received packet: " << rx_udp.rcv_str << endl << endl ;

		config->udp_pkt = rx_udp.rcv_str;
		rx_udp.rcv_str.clear();
		config->msg_waiting = true;
	}
}

bool send_prot(Tudp_handler *tx, string msg ){
	tx->send_str = msg ;
	   if ( tx->send_udp() )
	     {//--- got an error
	        cout << "   Send error: " << tx->error_message << endl ;
	        return(false) ;
	     }
	   //cout << "   Target IP is " << IP << ",  port " << PORT << endl ;
	   cout << "   Sent packet: " << tx->send_str << endl << endl ;

	   pkt_count++;

	   return (true);
}

bool send_cmd(Tudp_handler *tx, string msg ){
	tx->send_str = msg ;
	   if ( tx->send_udp() )
	     {//--- got an error
	        cout << "   Send error: " << tx->error_message << endl ;
	        return(false) ;
	     }
	   //cout << "   Target IP is " << IP << ",  port " << PORT << endl ;
	   cout << "   Sent packet: " << tx->send_str << endl << endl ;
	   return (true);
}

string int_to_string(int num){
	ostringstream ss;
	ss << num;
	return ss.str();
}


string to_string(char* str){
	ostringstream ss;
	ss << str;
	return ss.str();
}

bool start_log(string filename){
	logfile.close();//just in case
	logfile.open(filename.c_str());

	if (!logfile.is_open()){ // check file is open
		cout<< "Could not start Log"<<endl;
		return false;
	}
	logfile<< "Record of packet Latencey (in ms)"<< endl;
	return true;
}

bool write_log(long val){
	if (!logfile.is_open()){ // check file is open
		cout<< "Could not start Log"<<endl;
		return false;
	}
	cout << "write to file" << val << endl;
	logfile << val << "\n";
	//logfile.close();
	return true;
}

bool stop_log(){
	logfile.close();
	return true;
}


int main(int argc, char **argv) {

	long r_rate, default_rate, extra_rate;

	long status_age; // time since last refresh.
	bool sync_requested = false;


	struct timeval t_curr, t_last; // Used to calc need for refresh.
	struct timespec sleep_time ; // Time spec for sleeps.
	   sleep_time.tv_sec = 0 ;
	   sleep_time.tv_nsec = 0.5E3 ;// roughly works out to 1ms.


//<<<<<<<<<< Setup UDP Rx

 //// UDP port Structs setup:
	struct UDPPort cmd_port;
		cmd_port.IP = CMD_IP;
		cmd_port.Port = CMD_RX;
		cmd_port.udp_pkt = "";
		cmd_port.msg_waiting = false;

	struct UDPPort protocol_port;
		protocol_port.IP = PROT_IP;
		protocol_port.Port = PROT_RX;
		protocol_port.udp_pkt = "";
		protocol_port.msg_waiting = false;



//
// Thread Set-up:
//
	pthread_attr_t attr;	// Default attributes
	pthread_attr_init(&attr);
	pthread_t tid_cmd, tid_p;// two Id's for threads

	pthread_create(&tid_cmd, &attr, ltnr, &cmd_port);
	pthread_create(&tid_p, &attr, ltnr, &protocol_port);


//<<<<<<<<<< Setup UDP Tx
	Tudp_handler tx_udp_protocol( protocol_port.IP, PROT_TX) ;
	Tudp_handler tx_udp_cmd(cmd_port.IP, CMD_TX);
	cout << "load up:"<< cmd_port.IP<< endl;

	gettimeofday(&t_last, NULL); // get current time for comparison later should be part of loop activation;

//// Main Loop:

	// wait for a start signal
	bool active =false;

	ostringstream ss;

	string id = to_string(argv[1]);
	ss << REPLY_MSG << " D"<< id ;
	string reply_msg = ss.str();

	while(1)
	{
		// Sleep to free up CPU for other tasks
				nanosleep(&sleep_time, NULL) ;

		// Check Command port (this may tell loop to stop or report status)
		if (cmd_port.msg_waiting){
			string pkt;
			pkt.clear();

			pkt = cmd_port.udp_pkt;
			cout << "cleared?:" << pkt<< endl;
			cmd_port.msg_waiting =false;
			cmd_port.udp_pkt.clear();

			cout << "RX from "<< cmd_port.Port<< " : "<<pkt<<endl;

			//template string: Start <filename> R<Refresh_Rate>
			//		EG: Start ICD1-1510 R10
			if(!pkt.substr(0,5).compare("Start")){
				active = true;
				pkt_count =0;
				start_log(pkt.substr(6, pkt.find("R")-7));
				default_rate = r_rate = atoi(pkt.substr(pkt.find("R")+1).c_str())*1E6; // covert Sec to ms
				extra_rate = default_rate + 1E6; // lag behind a bit to wait for another request.

				gettimeofday(&t_last, NULL); // Rest timer.

				continue;
			}
			if(!pkt.compare("Stop")){
				stop_log();
				active = false;
				continue;
			}
			if(!pkt.compare("Report")){
				cout << cmd_port.IP<< " "<< (cmd_port.Port+2)<< endl;
				send_cmd(&tx_udp_cmd, int_to_string(pkt_count));
			}

		}	// end command port check.

		// check that the program has been activated:
		if (!active){

			continue;
		}

		// Check protocol port, this should include replies from IoT device or unrequested 	status updates

		if (protocol_port.msg_waiting){
			string pkt = protocol_port.udp_pkt;
			protocol_port.msg_waiting =false;

			cout << "RX from "<< protocol_port.Port<< " : "<< pkt<<endl;

			if(!pkt.substr(0,4).compare("Sync")){

				gettimeofday(&t_curr, NULL);
				long latency  = (t_curr.tv_sec  - t_last.tv_sec ) * 1000000 +
								(t_curr.tv_usec - t_last.tv_usec) -default_rate;
				if (latency < 0){
					latency =0;
				}
				write_log(latency); // record packet delay

				gettimeofday(&t_last, NULL);//reset timer


				if ((pkt.substr(6).compare(id))||(!sync_requested)){
					r_rate = extra_rate;
					cout << "Got a free Sync"<< endl;
				}
				else{
					cout << "got a Sync. I am the leader!!!!!!<<<<<<<<<<<<<<<<<<<<<"<< endl;
				}

				sync_requested = false;

			}
		}//end Protocol port


		// Check Refresh status:
		gettimeofday(&t_curr, NULL);
		status_age = (t_curr.tv_sec  - t_last.tv_sec ) * 1000000 +
				        (t_curr.tv_usec - t_last.tv_usec) ;
		if(sync_requested){
			//cout << "age: " << status_age << " compared to limit " << (r_rate+5E6)<< endl;
			if(status_age < (r_rate+2E6)){ // allow 1 sec for transmission
				continue;
			}
		}

		if(status_age >= r_rate){
			send_prot(&tx_udp_protocol, reply_msg);// reques Sync
			sync_requested = true;
			r_rate = default_rate;
		}// end refresh check



	}// end while

}//end main
